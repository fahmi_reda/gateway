<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->get('microservice/list', 'MicroserviceController@list');
$router->get('microservice/read/{id}', 'MicroserviceController@read');
$router->post('microservice/create', 'MicroserviceController@create');
$router->put('microservice/update/{id}', 'MicroserviceController@update');
$router->patch('microservice/update/{id}', 'MicroserviceController@update');
$router->delete('microservice/delete/{id}', 'MicroserviceController@delete');

$router->get('/', function () use ($router) {
    return $router->app->version();
});
