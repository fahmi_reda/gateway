<?php

namespace App\Http\Controllers;

// require_once __DIR__ . '/../../../vendor/autoload.php';


use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class RpcClient
{
    private $connection;
    private $channel;
    private $callback_queue;
    private $response;
    private $corr_id;


    public function __construct()
    {


        $this->connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest', '/');

        $this->channel = $this->connection->channel();
        list($this->callback_queue,,) = $this->channel->queue_declare(
            "",
            false,
            false,
            true,
            false
        );
        $this->channel->basic_consume(
            $this->callback_queue,
            '',
            false,
            true,
            false,
            false,
            array(
                $this,
                'onResponse'
            )
        );
    }

    public function onResponse($rep)
    {
        if ($rep->get('correlation_id') == $this->corr_id) {
            $this->response = $rep->body;
       
        }
    }

    public function call($body)
    {

        $time_start = microtime(true);
        $this->response = null;
        $this->corr_id = uniqid();

        $msg = new AMQPMessage(
            $body,
            array(
                'correlation_id' => $this->corr_id,
                'reply_to' => $this->callback_queue
            )
        );
        $this->channel->basic_publish($msg, '', 'rpc_queue');

        // $timeout = 5;
        // while (!$this->response) {
        //     try {
        //         $this->channel->wait(null, false, $timeout);
        //         return $this->response;
        //     } catch (\PhpAmqpLib\Exception\AMQPTimeoutException $e) {
        //         return 'timeout' . $this->request;
        //         $this->channel->close();
        //         $this->connection->close();
        //         exit;
        //     }
        // }
        while (!$this->response) {
            $this->channel->wait();
        }
        // return array(
        //     'resp' => $this->response,
        //     'time_start' => $time_start,
        //     'id' => $this->corr_id
        // );
        return $this->response;
    }
}


// $time_end=microtime(true);
// $time=$time_end-$response['time_start'];
// echo ' [.] Got ', $response['resp'], "\n","it took : ", $time, "seconds \n", "CORRELATION_ID :", $response['id'], "\n";
// $a = new RpcClient();
// for ($i = 0; $i < 10000; $i++) {
//     # code...
//     $a->call(json_encode(array(
//         'type' => 'list',
//     )));
// }
