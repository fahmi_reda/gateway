<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\RpcClient;

class MicroserviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        // return "hello world";
        $time_start = microtime(true);

        $body = array(
            'type' => 'list',
        );

        $RpcClient = new RpcClient();


        $response = $RpcClient->call(json_encode($body));
        $responseJson = json_decode($response)->original;

        //************ count time *****************************************/
        $time_end = microtime(true);
        $timeController = $time_end -  $time_start;



        // $response['time'] = $time_end -  $response['time_start'];



        return response(['data' => $responseJson->data, 'code' => $responseJson->code, 'time' => $timeController],);
        // return response(['data' => $a->original->data, 'time' => $timeController]);
        // return response(['data' => json_decode($response), 'time' => $timeController]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function read($id)
    {
        $time_start = microtime(true);

        $body = array(
            'type' => 'read',
            'id' => $id,
        );
        $RpcClient = new RpcClient();
        $response = $RpcClient->call(json_encode($body));

        $time_end = microtime(true);
        $timeController = $time_end -  $time_start;

        $responseJson = json_decode($response)->original;
        $code = $responseJson->code;
        if ($code == 404) {
            // return gettype($responseJson->code);

            return response(['error' => $responseJson->error, 'code' => $responseJson->code, 'time' => $timeController], $code);
        }
        return response(['data' => $responseJson->data, 'code' => $responseJson->code, 'time' => $timeController], $code);

        // return response(['data' => json_decode($response), 'time' => $time]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $time_start = microtime(true);

        $body = array(
            'type' => 'create',
            'data' => $request->all(),
        );
        $RpcClient = new RpcClient();
        $response = $RpcClient->call(json_encode($body));
        $time_end = microtime(true);
        $time = $time_end -  $time_start;
        return response(['data' => json_decode($response), 'time' => $time]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $time_start = microtime(true);

        $body = array(
            'type' => 'update',
            'id' => $id,
            'data' => $request->all(),
        );

        $RpcClient = new RpcClient();
        $response = $RpcClient->call(json_encode($body));
        $time_end = microtime(true);
        $time = $time_end -  $time_start;
        return response(['data' => json_decode($response), 'time' => $time]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $time_start = microtime(true);

        $body = array(
            'type' => 'delete',
            'id' => $id,

        );
        $RpcClient = new RpcClient();
        $response = $RpcClient->call(json_encode($body));
        $time_end = microtime(true);
        $time = $time_end -  $time_start;
        return response(['data' => json_decode($response, true), 'time' => $time]);
    }
}
